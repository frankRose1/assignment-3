"""
Assignment # 3 for the python course
Function will take in 3 parameters
if 2 or more params are equal, return True
return False if none are equal
"""


def compare(a, b, c):
    counter = 0
    if (a == b) or (a == c):
        counter += 1
    if (b == c) or (b == a):
        counter += 1
    if (c == a) or (c == b):
        counter += 1

    if counter >= 1:
        return True
    else:
        return False


result1 = compare('two', 'two', 'three')  # True
result2 = compare(1, 2, 3)  # False
result3 = compare(1.5, 2.4, '5.4')  # False
result4 = compare(10, False, 10)  # True

print(result1)
print(result2)
print(result3)
print(result4)
